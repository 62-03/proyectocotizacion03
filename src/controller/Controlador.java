package controller;

import models.Cotizacion;
import models.DbCotizacion;
import view.dlgCotizaciones;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.Port;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JDialog;
import javax.swing.JOptionPane;


public class Controlador implements ActionListener {

    DbCotizacion db;
    Cotizacion cot;
    dlgCotizaciones vista;

    private boolean isInsertar = false;

    public Controlador(DbCotizacion db, Cotizacion cot, dlgCotizaciones vista) {
        this.db = db;
        this.cot = cot;
        this.vista = vista;

        vista.btnNuevo.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnBuscar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnBorrar.addActionListener(this);
    }

    private void iniciarVista() throws Exception {
        vista.jtListaAlumnos.setModel(db.listar());
        vista.setTitle(":: COTIZACION ::");
        vista.setSize(700, 700);
        vista.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.btnNuevo) {
            this.habilitar();
            this.isInsertar = true;
        } else if (e.getSource() == vista.btnGuardar) {
            try {
                cot.setNumCotizacion(vista.txtNumCotizacion.getText());
                cot.setDescripcionAutomovil(vista.txtDescripcion.getText());
                cot.setPorcentajePago(Integer.parseInt(vista.spnPorcentaje.getValue().toString()));
                cot.setPrecioAutomovil(Integer.parseInt(vista.txtPrecio.getText()));

                if (vista.rdb12.isSelected()) {
                    cot.setPlazo(12);
                }
                if (vista.rdb24.isSelected()) {
                    cot.setPlazo(24);
                }
                if (vista.rdb36.isSelected()) {
                    cot.setPlazo(36);
                }
                if (vista.rdb48.isSelected()) {
                    cot.setPlazo(48);
                }
                if (vista.rdb60.isSelected()) {
                    cot.setPlazo(60);
                }
                
                if (this.isInsertar == true) {
                    db.insertar(cot);
                    JOptionPane.showMessageDialog(vista, "Se agrego con exito");
                    limpiar();

                    this.deshabilitar();
                } else {
                    db.actualizar(cot);
                    JOptionPane.showMessageDialog(vista, "Se actualizo con exito");
                    limpiar();
                    this.deshabilitar();
                }
                
                vista.jtListaAlumnos.setModel(db.listar());
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "No se pudo guardar, surgio el siguiente error: " + ex.getMessage());
            } catch (Exception ex2) {
                JOptionPane.showMessageDialog(vista, "No se pudo guardar, surgio el siguiente error: " + ex2.getMessage());
            }
            
            
        } else if (e.getSource() == vista.btnBuscar) {
            cot.setNumCotizacion(vista.txtNumCotizacion.getText());
            
            try {
                //cot = (Cotizacion) db.consultar(vista.txtNumCotizacion.getText());
                cot = (Cotizacion) db.buscar(vista.txtNumCotizacion.getText());
            } catch (Exception ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            if (cot.getIdCotizacion() == -1) {
                JOptionPane.showMessageDialog(vista, "No existe");
                this.limpiar();
                this.deshabilitar();
            } else {
                vista.txtDescripcion.setText(cot.getDescripcionAutomivil());
                vista.txtPrecio.setText(String.valueOf(cot.getPrecioAutomovil()));

                switch (cot.getPlazo()) {
                    case 12:
                        vista.rdb12.setSelected(true);
                        break;
                    case 24:
                        vista.rdb24.setSelected(true);
                        break;
                    case 35:
                        vista.rdb36.setSelected(true);
                        break;
                    case 48:
                        vista.rdb48.setSelected(true);
                        break;
                    case 60:
                        vista.rdb60.setSelected(true);
                        break;
                }

                vista.spnPorcentaje.setValue(cot.getPorcentajePago());
                vista.lblPagoInicial.setText(String.valueOf(cot.calcularPagoInicial()));
                vista.lblTotalFin.setText(String.valueOf(cot.calcularTotalAfinanciar()));
                vista.lblPagoMensual.setText(String.valueOf(cot.calcularPagoMensual()));
                this.isInsertar = false;
            }

        } else if (e.getSource() == vista.btnBorrar) {
            int opcion = 0;
            cot.setNumCotizacion(vista.txtNumCotizacion.getText());

            opcion = JOptionPane.showConfirmDialog(vista, "¿Desea borrar el registro?", "Cotizacion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

            if (opcion == JOptionPane.YES_OPTION) {
                try {
                    db.borrar(cot.getNumCotizacion());
                    JOptionPane.showMessageDialog(vista, "Se borro el registro con exito");
                    vista.jtListaAlumnos.setModel(db.listar());
                    this.limpiar();
                    this.deshabilitar();
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(vista, "No se pudo guardar, surgio el siguiente error: " + ex.getMessage());
                } catch (Exception ex2) {
                    JOptionPane.showMessageDialog(vista, "No se pudo guardar, surgio el siguiente error: " + ex2.getMessage());
                }
            }

        } else if (e.getSource() == vista.btnLimpiar) {
            this.limpiar();
        } else if (e.getSource() == vista.btnCancelar) {
            this.limpiar();
            this.deshabilitar();
        } else if (e.getSource() == vista.btnCerrar) {
            int opcion = 0;

            opcion = JOptionPane.showConfirmDialog(vista, "¿Desea salir?", "Cotizacion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

            if (opcion == JOptionPane.YES_OPTION) {
                System.exit(1);
            }
        }
    }

    public void habilitar() {
        vista.txtNumCotizacion.setEnabled(true);
        vista.txtDescripcion.setEnabled(true);
        vista.txtPrecio.setEnabled(true);
        vista.spnPorcentaje.setEnabled(true);
        vista.pnlPlazos.setEnabled(true);
        vista.btnGuardar.setEnabled(true);
        vista.btnBorrar.setEnabled(true);
        vista.btnBuscar.setEnabled(true);
        vista.btnCancelar.setEnabled(true);
        vista.btnCerrar.setEnabled(true);
        vista.btnLimpiar.setEnabled(true);       
        vista.pnlPlazos.setEnabled(true);
        vista.rdb12.setEnabled(true);
        vista.rdb24.setEnabled(true);
        vista.rdb36.setEnabled(true);
        vista.rdb48.setEnabled(true);
        vista.rdb60.setEnabled(true);
    }

    public void deshabilitar() {
        vista.txtNumCotizacion.setEnabled(false);
        vista.txtDescripcion.setEnabled(false);
        vista.txtPrecio.setEnabled(false);
        vista.spnPorcentaje.setEnabled(false);
        vista.pnlPlazos.setEnabled(false);
        vista.btnGuardar.setEnabled(false);
        vista.btnBorrar.setEnabled(false);
        vista.btnBuscar.setEnabled(false);
        vista.btnCancelar.setEnabled(false);
        vista.btnCerrar.setEnabled(false);
        vista.btnLimpiar.setEnabled(false);       
        vista.pnlPlazos.setEnabled(false);
    }

    public void limpiar() {
        vista.txtNumCotizacion.setText("");
        vista.txtDescripcion.setText("");
        vista.txtPrecio.setText("");
        vista.spnPorcentaje.setValue(0);
        vista.rdb12.setSelected(true);
        vista.lblPagoInicial.setText("");
        vista.lblPagoMensual.setText("");
        vista.lblTotalFin.setText("");
    }

    public static void main(String[] args) throws Exception {
        DbCotizacion db = new DbCotizacion();
        Cotizacion cot = new Cotizacion();
        dlgCotizaciones vista = new dlgCotizaciones();
        Controlador con = new Controlador(db, cot, vista);
        con.iniciarVista();

        /*cot.setNumCotizacion("123");
        cot.setDescripcionAutomovil("Toyota Cambry");
        cot.setPrecioAutomovil(350000);
        cot.setPlazo(60);
        cot.imprimirCotizacion();

        db.insertar(cot);*/
    }
}
