package models;
import view.dlgCotizaciones;
import models.Cotizacion;
import models.DbManejador;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;
import javax.swing.JOptionPane;
import models.DbPersistencia;

public class DbCotizacion extends DbManejador implements DbPersistencia {
    dlgCotizaciones vista;
    
    @Override
    public void insertar(Object objecto) throws Exception {
        Cotizacion cot = new Cotizacion();
        cot = (Cotizacion) objecto;

        String consulta = "Insert into " + "sistema.cotizaciones(idCotizacion, numCotizacion, descripcion, precio, porcentaje, plazos)values(?,?,?,?,?,?)";
        if (this.conectar()) {
            try {

                this.sqlConsulta = conexion.prepareStatement(consulta);
                this.sqlConsulta.setInt(1, cot.getIdCotizacion());
                this.sqlConsulta.setString(2, cot.getNumCotizacion());
                this.sqlConsulta.setString(3, cot.getDescripcionAutomivil());
                this.sqlConsulta.setInt(4, cot.getPrecioAutomovil());
                this.sqlConsulta.setInt(5, cot.getPorcentajePago());
                this.sqlConsulta.setInt(6, cot.getPlazo());
                this.sqlConsulta.executeUpdate();
                this.desconectar();

            } catch (SQLException e) {
                JOptionPane.showMessageDialog(vista,"Surgio un error al insertar" +e.getMessage());
            }
        } else {
            JOptionPane.showMessageDialog(vista,"No fue posible conectarse");
        }
    }

    @Override
    public void actualizar(Object objecto) throws Exception {
        Cotizacion cot = new Cotizacion();
        cot = (Cotizacion) objecto;
        String consulta = "UPDATE sistema.cotizaciones SET descripcion = ?, precio = ?, porcentaje = ?, plazos = ? WHERE numCotizacion = ?";
        if (this.conectar()) {
            try {
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                this.sqlConsulta.setString(1, cot.getDescripcionAutomivil());
                this.sqlConsulta.setInt(2, cot.getPrecioAutomovil());
                this.sqlConsulta.setInt(3, cot.getPorcentajePago());
                this.sqlConsulta.setInt(4, cot.getPlazo());
                this.sqlConsulta.setString(5, cot.getNumCotizacion());
                this.sqlConsulta.executeUpdate();
                this.desconectar();
                JOptionPane.showMessageDialog(vista,"Se actualizó correctamente");

            }catch (SQLException e) {
                JOptionPane.showMessageDialog(vista,"Surgio un error al actualizar" +e.getMessage());
            }
        }
    }
    
    @Override
    public DefaultTableModel listar() throws Exception {
        Cotizacion cot;
        if(this.conectar()){ 
            String sql = "select * from sistema.cotizaciones order by numCotizacion";
            DefaultTableModel Tabla = new DefaultTableModel();
            PreparedStatement pst = conexion.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            ResultSetMetaData Datos = rs.getMetaData();

            //Agregar Columnas
            for (int column = 1; column <= Datos.getColumnCount(); column++) {
                Tabla.addColumn(Datos.getColumnLabel(column));
            }
            //Agregar Tablas
            while (rs.next()) {
                    Object[] row = new Object[Datos.getColumnCount()];
                    for (int column = 1; column <= Datos.getColumnCount(); column++) {
                        row[column - 1] = rs.getString(column);
                    }
                    Tabla.addRow(row);
                }
            return Tabla;       
        }         
        return null;
    }

    @Override
    public Object buscar(String codigo) throws Exception {
        Cotizacion cot = new Cotizacion();
        if(this.conectar()){
            String consultar = "Select * from sistema.cotizaciones where numCotizacion= ?";
            this.sqlConsulta = this.conexion.prepareStatement(consultar);
            this.sqlConsulta.setString(1, codigo);
            this.Registros = this.sqlConsulta.executeQuery();
            if(this.Registros.next()){
                cot.setNumCotizacion(this.Registros.getString("numCotizacion"));
                cot.setDescripcionAutomovil(this.Registros.getString("descripcion"));
                cot.setPrecioAutomovil(this.Registros.getInt("precio"));
                cot.setPorcentajePago(this.Registros.getInt("porcentaje"));
                cot.setPlazo(this.Registros.getInt("plazos"));

            }
        }
        this.desconectar();
        return cot;
    }
    @Override
    public void borrar(String codigo) throws Exception {
        if(this.conectar()){
            try {
                String consultar = "DELETE FROM sistema.cotizaciones WHERE numCotizacion = ?";
                sqlConsulta = this.conexion.prepareStatement(consultar);
                sqlConsulta.setString(1, codigo);
                sqlConsulta.executeUpdate();
            } catch(SQLException e){
                System.out.println("Error al borrar: "+e.getMessage());
            }
        }
        
    }

    @Override
    public boolean isExiste(String codigo) throws Exception {
        boolean res = false;
        Cotizacion cot = new Cotizacion();
        if (this.conectar()) {
            String consulta = "select * from productos where codigo =?";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);
            this.Registros = this.sqlConsulta.executeQuery();
            if (this.Registros.next()) {
                res = true;
            }
        }
        this.desconectar();
        return res;
    }

    @Override
    public Object consultar(String numCoti) throws Exception {
        Cotizacion cot = new Cotizacion();
        if(this.conectar()){
            String consultar = "Select * from sistema.cotizaciones where numCotizacion= ?";
            this.sqlConsulta = this.conexion.prepareStatement(consultar);
            this.sqlConsulta.setString(1, numCoti);
            this.Registros = this.sqlConsulta.executeQuery();
            if(this.Registros.next()){
                cot.setNumCotizacion(this.Registros.getString("numCotizacion"));
                cot.setDescripcionAutomovil(this.Registros.getString("descripcion"));
                cot.setPrecioAutomovil(this.Registros.getInt("precio"));
                cot.setPorcentajePago(this.Registros.getInt("porcentaje"));
                cot.setPlazo(this.Registros.getInt("plazos"));

            }
        }
        this.desconectar();
        return cot;
    }
 
}