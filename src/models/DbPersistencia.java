/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package models;

import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
public interface DbPersistencia {
    public void insertar(Object objecto) throws Exception;
    public void actualizar(Object objecto) throws Exception;
    public boolean isExiste(String codigo) throws Exception;
    public DefaultTableModel listar() throws Exception;
    public Object consultar(String numCoti) throws Exception;
    public Object buscar(String codigo) throws Exception;
    public void borrar(String codigo) throws Exception;
}
